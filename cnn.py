import numpy as np
import pandas as pd
import keras
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from keras.models import Sequential
from keras.layers import Dense, Conv1D, Flatten

def cnn(x_train,x_test,y_train,y_test,scaler):
    rs = []
    cvr2 = []
    cvmse = []
    cvmae = []
    pdn_list = []

    #Convert to numpy arrays
    x_train_vals = x_train.values
    y_train_vals = y_train.values
    x_test_vals = x_test.values
    y_test_vals = y_test.values

    #Reshape x vectors into a format Keras recognizes
    x_train_vals = x_train_vals.reshape(x_train_vals.shape[0],1,x_train_vals.shape[1])
    x_test_vals = x_test_vals.reshape(x_test_vals.shape[0],1,x_test_vals.shape[1])

    model = Sequential()
    # model.add(InputLayer(batch_shape=(None,7)))
    model.add(Conv1D(filters=16, kernel_size=1, activation='tanh', input_shape=(1,7)))
    model.add(Flatten())
    model.add(Dense(1, activation='linear'))

    tbCallBack = keras.callbacks.TensorBoard(log_dir='/tmp/keras_logs', write_graph=True)
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['accuracy', 'mean_squared_error', 'mean_absolute_error'])
    model.fit(x_train_vals, y_train_vals, epochs=100, batch_size=2, verbose=1, validation_split=0.3, callbacks=[tbCallBack])
    pdn = model.predict(x_test_vals)

    mse = mean_squared_error(y_test.values, pdn)
    mae = mean_absolute_error(y_test.values, pdn)
    r2 = r2_score(y_test.values,pdn)
    print("scaled convolutional neural network\nrmse: (%.5f) mae: (%.5f) r2: (%.5f)" % (mse**0.5, mae,r2))
    pdn = unscale(pdn,scaler)
    act = unscale(y_test.values,scaler)
    mse = mean_squared_error(act,pdn)
    mae = mean_absolute_error(act,pdn)
    r2 = r2_score(act,pdn)
    print("unscaled convolutional neural network\nrmse: (%.5f) mae: (%.5f) r2: (%.5f)" % (mse**0.5,mae,r2))

    plt.scatter(y_test,pdn, s=1, color="blue")
    plt.title("Surge prediction results using CNN")
    plt.xlabel("y_test")
    plt.ylabel("pdn")
    plt.savefig("m_nn_results.png")

    dfPdn = pd.DataFrame(np.column_stack([pdn,act]), columns=['pdn', 'act'])
    dfPdn.to_csv("cnn_pdn.csv")

def unscale(column,scaler):
    no = len(column)
    zeroarr = np.zeros(no)
    nparr = np.column_stack([zeroarr, zeroarr, zeroarr, zeroarr, zeroarr, zeroarr, zeroarr, column])
    nparr = scaler.inverse_transform(nparr)
    npcol = nparr[:,7]
    return npcol

seed = 7
np.random.seed(seed)
# train dataset creation
df_nola = pd.read_csv('nola_elevation_trimmed.csv')
df_nola = df_nola[df_nola['pressure'] > 0]
# df_nola['elevation'] = elevation(df_nola['lat_x'],df_nola['lon_x'])
# df_nola = df_nola[df_nola['elevation'] > 0]
print(df_nola.year.unique())
df_nola = df_nola[['lat_x', 'lon_x', 'wspeed', 'pressure', 'lat_y', 'lon_y','elevation','target']]
scaler=MinMaxScaler()
# df_nola = pd.DataFrame(normalize(df_nola),columns=df_nola.columns)
df_nola = pd.DataFrame(scaler.fit_transform(df_nola),columns=df_nola.columns)
df_nola_Y = df_nola[['target']]
df_nola_X = df_nola[['lat_x', 'lon_x', 'wspeed', 'pressure', 'elevation','lat_y', 'lon_y',]]
X_train, X_test, y_train, y_test = train_test_split(df_nola_X, df_nola_Y, test_size=0.25)

"""FOR USING 2005 AS TEST DATA
#test dataset creation
df_nola_test = pd.read_csv('nola_elevation_trimmed.csv')
df_nola_test = df_nola_test[df_nola_test['pressure'] > 0]
df_nola_test = df_nola_test[df_nola_test['year'] == 2005]
# df_nola_test['elevation'] = elevation(df_nola_test['lat_x'],df_nola_test['lon_x'])
# df_nola_test = df_nola_test[df_nola_test['elevation'] > 0]
df_nola_test = df_nola_test[['lat_x', 'lon_x', 'wspeed', 'pressure', 'elevation','lat_y', 'lon_y','target']]
scaler=MinMaxScaler()
df_nola_test = pd.DataFrame(normalize(df_nola_test),columns=df_nola_test.columns)
# df_nola = pd.DataFrame(scaler.fit_transform(df_nola),columns=df_nola.columns)
df_nola_testY = df_nola_test[['target']]
df_nola_testX = df_nola_test[['lat_x', 'lon_x', 'wspeed', 'pressure', 'elevation','lat_y', 'lon_y',]]
#model calling
# m_svr(df_nola_X,df_nola_testX,df_nola_Y,df_nola_testY)
# nn(df_nola_X,df_nola_testX,df_nola_Y,df_nola_testY)
"""

cnn(X_train, X_test, y_train, y_test,scaler)
