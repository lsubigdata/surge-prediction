import json
import numpy
import psycopg2 as DB
import pprint
import sys,os
import pandas as pd


qlon = sys.argv[1]
qlat = sys.argv[2]
dist_miles = sys.argv[3]

conn = DB.connect('dbname=surgedb user=srccdb host=hail.srcc.lsu.edu')
cur = conn.cursor()


def get_vals(qlat, qlon, radius):
    qlat = float(qlat)
    qlon = float(qlon)
    radius = radius
    q_str = "select storm_name,year,surge_ft,stormtide_ft,surge_m,stormtide_m,date,location,s.lat,s.lon,h.obsdate,h.obstime,h.obsnote,h.strength,h.wspeed,h.pressure,h.lat,h.lon from surge_events s inner join hurdat h on UPPER(TRIM(s.storm_name)) = UPPER(TRIM(h.sname)) AND substr(s.year,1,4) = substr(h.obsdate,1,4) WHERE ST_Distance(ST_Transform(ST_GeomFromText('POINT(%s %s)',4326),%s), ST_Transform(location,%s)) ;" %(qlon, qlat, radius,radius)
    cur.execute(q_str)
    results = cur.fetchall()
    df = pd.DataFrame(results)
    home = os.getcwd()
    opath = home + str(dist_miles) + 'miFromLat' + str(qlat) + 'Lon' + str(qlon) + '.csv'
    df.to_csv(opath)
    print(df.head())


if __name__ == '__main__':
    v = get_vals(qlat, qlon, float(dist_miles) * 1.609 * 1000)
