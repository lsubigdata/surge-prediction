import psycopg2 as db
import numpy as np
import pandas as pd
import sys
import json
import keras
from keras.models import Model
from keras.layers import Conv1D, Dense, Input, Reshape, Flatten
# from keras.callbacks import TensorBoard, Callback
from keras.optimizers import Adam

city = sys.argv[1]
qlat = sys.argv[2]
qlon = sys.argv[3]
surge_miles = float(sys.argv[4])
hurricane_miles = float(sys.argv[5])

earthRadius=6378.0
key='Apq0so7LzTSJk6E3D8sFkOwwxG23Y1NO0UGDaIusUzQ-tINLKo87gVWP5FEVRjox'

conn = db.connect('dbname=surgedb host=hail.srcc.lsu.edu user=srccdb')
cur = conn.cursor()

#Get unique storms that passed through the area
def get_storms(hur_miles):
    dist_in_deg=57.2958 * hur_miles * 1.609 / earthRadius
    qstr1='''select distinct sname, substring(scode from 5 for 4) from hurdat where ST_DWithin(hurdat_loc,ST_GeomFromText('POINT({} {})',4326),{})'''.format(qlon,qlat,dist_in_deg)
    cur.execute(qstr1)
    rows=cur.fetchall()
    colnames = ['sname', 'syear']
    df1=pd.DataFrame(rows)
    df1.columns = colnames

    return df1

#Get surge values into the dataframe
def get_surge_data(surge_miles):
    dist_in_deg=57.2958 * surge_miles * 1.609 / earthRadius
    qstr1='''select storm_name, year, surge_ft, surge_m, stormtide_ft, stormtide_m, lat, lon from surge_events where ST_DWithin(surge_loc,ST_GeomFromText('POINT({} {})',4326),{})'''.format(qlon,qlat,dist_in_deg)
    cur.execute(qstr1)
    rows=cur.fetchall()
    colnames = ['sname', 'syear', 'su_ft', 'su_m', 'st_ft', 'st_m', 'su_lat', 'su_lon']
    df2=pd.DataFrame(rows)
    df2.columns = colnames

    return df2

def set_target(df2):
    #Replace blank values with np.nan
    df2['su_ft'] = df2['su_ft'].replace(r'^\s*$', np.nan, regex=True)
    df2['st_ft'] = df2['st_ft'].replace(r'^\s*$', np.nan, regex=True)
    df2['su_m'] = df2['su_m'].replace(r'^\s*$', np.nan, regex=True)
    df2['st_m'] = df2['st_m'].replace(r'^\s*$', np.nan, regex=True)

    #Set a unified target column
    tg = []

    for i in range(0,len(df2)):
        if(np.isfinite(float(df2['su_ft'][i]))):
            tg.append(float(df2['su_ft'][i]))
        elif(np.isfinite(float(df2['su_m'][i]))):
            tg.append(float(df2['su_m'][i])*3.28)
        elif(np.isfinite(float(df2['st_ft'][i]))):
            tg.append(float(df2['st_ft'][i]))
        elif(np.isfinite(float(df2['st_m'][i]))):
            tg.append(float(df2['st_m'][i])*3.28)
        else:
            tg.append(np.nan)

    df2['target'] = tg
    df2 = df2.drop(['su_ft','st_ft', 'su_m', 'st_m'], axis=1)
    df2 = df2[np.isfinite(df2['target'])]

    #This is slow (especially for a string concatenation), but it works...
    df2['scode'] = np.nan
    for i in range(0, len(df2)):
        df2['scode'].iloc[i] = str(df2['sname'].iloc[i]).upper() + str(df2['syear'].iloc[i])

    return df2

def get_elevation(df2):
    #Get unique lat/longs to reduce API hits
    df3 = df2.groupby(['lat_x', 'lon_x']).size().reset_index(name='Freq')
    print(df3.head())
    locations = []
    for a in range(0, len(df3)):
        locations.append([df3['lat_x'].iloc[a], df3['lon_x'].iloc[a]])

    #Get elevation data
    elev = []
    for loc in locations:
        try:
            r = requests.get('http://dev.virtualearth.net/REST/v1/Elevation/List?points={},{}&key={}'.format(loc[0],loc[1],key))
            rjson = r.json()
            print(rjson['resourceSets'][0]['resources'][0])
            elev.append(float(rjson['resourceSets'][0]['resources'][0]['elevations'][0])*3.28)
            print("Obtained elevation {} of {}".format(len(elev), len(locations)))
            sleep(1)
        except:
            print("Error for location: {}".format(loc,))

    df3['elevation'] = elev
    df2 = pd.merge(df2, df3, on=['lat_x', 'lon_x'], how='outer')

    df2.to_csv('./{}_Surge_Dataset.csv'.format(city,))
    df2 = df2.drop(['Freq'], axis=1)
    return df2

#Get storm data into a dictionary. One entry containing the entire data for each storm.
def set_up_hurdat(df):
    storm_dict = {}

    for i in range(0, len(df)):
        cur.execute("SELECT wspeed, pressure, lat, lon FROM hurdat WHERE sname='{}' AND substring(scode from 5 for 4) = '{}' AND obsnote = 'NaN';".format(df['sname'].iloc[i], df['syear'].iloc[i]))
        rows = cur.fetchall()
        df_tmp = pd.DataFrame(rows)
        df_tmp.columns = ['wspeed', 'pressure', 'lat', 'lon']

        df_tmp_vals = df_tmp.values
        key_str = df['sname'].iloc[i] + str(df['syear'].iloc[i])
        storm_dict[key_str] = df_tmp_vals

    return storm_dict

#Go through df2. For each entry in df2, append the storm history list.
#Pandas does not like it if you try to put the list in a dataframe cell.
def create_hurdat_dataset(dictionary, df):
    output_list = []
    output_list.append(0)
    for c in range(len(df),0,-1): #Go in reverse order so that indices don't get messed up
        try:
            # storm_list = list(dictionary[df['scode'].iloc[c-1]])
            storm_list = dictionary[df['scode'].iloc[c-1]]
            output_list.append(storm_list)
        except:
            df = df.drop(df.index[c-1])
            pass
        print(str(c-1) + " entries to go")

    output_list = output_list[::-1] #reverse the order to match the original order
    output_list = output_list[:-1] #remove the dummy zero that was put in to account for zero-indexing

    return df, output_list

def model_creation(df, hurdat_list):
    dfX = df[['su_lat', 'su_lon']]
    #dfX = df['su_lat', 'su_lon', 'elev']
    dfY = df['target']

    dfX = dfX.values
    dfY = dfY.values
    hurdat_array = np.asarray(hurdat_list)
    print("HURDAT_ARRAY SHAPE")
    print(hurdat_array.shape)
    print("HURDAT_ARRAY[0]")
    print(hurdat_array[0])
    print("HURDAT_ARRAY[0][0]")
    print(hurdat_array[0][0])
    stormDataInput = keras.preprocessing.sequence.pad_sequences(hurdat_array, maxlen=133, padding='post', value=[np.nan, np.nan, np.nan, np.nan])
    print(stormDataInput[0])
    stormDataInputSeq = Input(shape=(133, len(hurdat_array[0][0])), name='StormDataInput')
    stormDataOutput = Conv1D(filters=50, kernel_size=5, activation='hard_sigmoid')(stormDataInputSeq)
    print(stormDataOutput.shape)
    sDOFlat = Flatten()(stormDataOutput)
    sDOReshape = Reshape((129*50,))(sDOFlat)
    surgeLocInput = Input(shape=(dfX.shape[1],), name='SurgeLocInput')
    conc = keras.layers.concatenate([sDOReshape, surgeLocInput])
    # conc = keras.layers.concatenate([stormDataOutput, surgeLocInput])
    dense = Dense(1, activation='linear')(conc)
    model = Model(inputs=[stormDataInput, surgeLocInput], outputs=[dense])
    model.compile(optimizer=Adam(lr=0.001), loss='mean_squared_error', metrics=['mse', 'mae'])

    # hist = model.fit([hurdat_array, dfX], [dfY], batch_size=256, epochs=100, shuffle=True, verbose=1, validation_split=0.2)
    hist = model.fit([stormDataInput, dfX], [dfY], batch_size=256, epochs=100, shuffle=True, verbose=1, validation_split=0.2)
    df_hist = pd.DataFrame(hist.history)
    df_hist.to_csv('./tmp/df_hist.csv')

    return model

df1 = get_storms(hurricane_miles)
df2 = get_surge_data(surge_miles)
df2 = set_target(df2)
# df2 = get_elevation(df2) #Don't uncomment until I'm sure I'm ready
hurdat_dict = set_up_hurdat(df1)
df3, hurdat_list = create_hurdat_dataset(hurdat_dict, df2)
print("LENGTHS - DF2: {}, DF3: {}, HURDAT_LIST: {}".format(len(df2), len(df3), len(hurdat_list)))
model = model_creation(df3, hurdat_list)
