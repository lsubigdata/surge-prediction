import psycopg2 as DB
import pandas as pd
import numpy as np
import sys
import requests
import json
import os.path
import matplotlib.pyplot as plt
import keras
import urllib.request as ur
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.layers.normalization import BatchNormalization
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score, KFold, train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.svm import SVR
from time import sleep

conn=DB.connect('dbname=surgedb host=hail.srcc.lsu.edu user=srccdb')
#key one(??? Google): AIzaSyAiMVHtoLBrJUvKjspMqlIGUQJVKG8yvss
#key two(Eric Google): AIzaSyB462WcWJbHfwl_MqsiXgAnLhBmkYm9cII
#key three(Bing): Apq0so7LzTSJk6E3D8sFkOwwxG23Y1NO0UGDaIusUzQ-tINLKo87gVWP5FEVRjox
key='Apq0so7LzTSJk6E3D8sFkOwwxG23Y1NO0UGDaIusUzQ-tINLKo87gVWP5FEVRjox'

city = sys.argv[1]
qlat = sys.argv[2]
qlon = sys.argv[3]
surge_miles = float(sys.argv[4])
hur_miles = float(sys.argv[5])
earthRadius=6378.0

#Get surge observations
def get_surge_obs(surge_miles):
    dist_in_deg=57.2958 * surge_miles * 1.609 / earthRadius
    qstr='''select * from surge_events where ST_DWithin(surge_loc,ST_GeomFromText('POINT({} {})',4326),{})'''.format(qlon,qlat,dist_in_deg)
    cur.execute(qstr)
    rows=cur.fetchall()
    colnames = [desc[0] for desc in cur.description]
    print(colnames)
    df=pd.DataFrame(rows)
    df.columns = colnames
    df['storm_name']=df['storm_name'].str.upper()
    return df

#Get hurdat observations
def get_hurdat_obs(hur_miles):
    dist_in_deg=57.2958 * hur_miles * 1.609 / earthRadius
    qstr1='''select * from hurdat where ST_DWithin(hurdat_loc,ST_GeomFromText('POINT({} {})',4326),{})'''.format(qlon,qlat,dist_in_deg)
    cur.execute(qstr1)
    rows=cur.fetchall()
    colnames = [desc[0] for desc in cur.description]
    print(colnames)
    df1=pd.DataFrame(rows)
    colnames[-2]='storm_name'
    df1.columns = colnames
    df1['year']=df1['obsdate'].str[:4]
    return df1

def set_target(df2):
    #Replace blank values with np.nan
    df2['surge_ft'] = df2['surge_ft'].replace(r'^\s*$', np.nan, regex=True)
    df2['stormtide_ft'] = df2['stormtide_ft'].replace(r'^\s*$', np.nan, regex=True)
    df2['surge_m'] = df2['surge_m'].replace(r'^\s*$', np.nan, regex=True)
    df2['stormtide_m'] = df2['stormtide_m'].replace(r'^\s*$', np.nan, regex=True)

    #Set a unified target column
    tg = []

    for i in range(0,len(df2)):
        if(np.isfinite(float(df2['surge_ft'][i]))):
            tg.append(float(df2['surge_ft'][i]))
        elif(np.isfinite(float(df2['stormtide_ft'][i]))):
            tg.append(float(df2['stormtide_ft'][i]))
        elif(np.isfinite(float(df2['surge_m'][i]))):
            tg.append(float(df2['surge_m'][i])*3.28)
        elif(np.isfinite(float(df2['stormtide_m'][i]))):
            tg.append(float(df2['stormtide_m'][i])*3.28)
        else:
            tg.append(np.nan)

    df2['target'] = tg
    df2 = df2.drop(['surge_ft','stormtide_ft', 'surge_m', 'stormtide_m'], axis=1)
    df2 = df2[np.isfinite(df2['target'])]
    df2 = df2[df2['pressure'] > 0]
    return df2

def get_elevation(df2):
    #Get unique lat/longs to reduce API hits
    df3 = df2.groupby(['lat_x', 'lon_x']).size().reset_index(name='Freq')
    print(df3.head())
    locations = []
    for a in range(0, len(df3)):
        locations.append([df3['lat_x'].iloc[a], df3['lon_x'].iloc[a]])

    #Get elevation data
    elev = []
    for loc in locations:
        try:
            r = requests.get('http://dev.virtualearth.net/REST/v1/Elevation/List?points={},{}&key={}'.format(loc[0],loc[1],key))
            rjson = r.json()
            print(rjson['resourceSets'][0]['resources'][0])
            elev.append(float(rjson['resourceSets'][0]['resources'][0]['elevations'][0])*3.28)
            print("Obtained elevation {} of {}".format(len(elev), len(locations)))
            sleep(1)
        except:
            print("Error for location: {}".format(loc,))

    df3['elevation'] = elev
    df2 = pd.merge(df2, df3, on=['lat_x', 'lon_x'], how='outer')

    df2.to_csv('./{}_Surge_Dataset.csv'.format(city,))
    df2 = df2.drop(['Freq'], axis=1)
    return df2

def m_svr(x_train,x_test,y_train,y_test,scaler):
    print("SVR Block called")
    cvr2 = []
    cvmse = []
    cvmae = []
    print("Results arrays initialized")

    clf = SVR(kernel='rbf',epsilon=0.004,gamma=0.96,C=200, cache_size=2048) #was C=35k, gamma=.6
    print("Classifier defined")
    # scores = cross_val_score(clf,x_train,y_train, cv=5)
    # print("cross val score")
    # print(scores)
    clf.fit(x_train, y_train)
    print("Model is fit")

    pdn = clf.predict(x_test)
    print("Predictions are made")

    act = unscale(y_test,scaler)
    pdn = unscale(pdn,scaler)
    print("Data unscaled")
    # print("Score: " + str(clf.score(pdn,act)))

    cvr2.append(r2_score(act, pdn))
    cvmse.append(mean_squared_error(act, pdn))
    cvmae.append(mean_absolute_error(act, pdn))
    print("Predictions evaluated")
    print("SVR\nrmse: (%.2f) mae:(%.2f) " % (mean_squared_error(act,pdn)**0.5, mean_absolute_error(act,pdn)))

    #plt.scatter(X_train,y_train,color="red")
    plt.scatter(act,pdn, s=1, color="blue")
    plt.title("Surge prediction results using SVR")
    plt.xlabel("Actual")
    plt.ylabel("Predicted")
    plt.savefig("{}_SVR_sample.png".format(city,))
    #plt.close()
    print("Plot generated")

    dfRes = pd.DataFrame(np.column_stack([cvr2, cvmse, cvmae]), columns=['R2', 'MSE', 'MAE'])
    dfRes.to_csv('{}_SVR_.csv'.format(city,))
    dfPdn = pd.DataFrame(np.column_stack([pdn,act]),columns=['pdn','y_test'])
    dfPdn.to_csv("{}_svr_pdn.csv".format(city,))

def nn(x_train,x_test,y_train,y_test,scaler):
    model = Sequential()
    # Original paper model
    # model.add(Dense(16,input_dim=7,kernel_initializer='normal'))#was 20 nodes
    # model.add(Activation('tanh'))
    # model.add(Dense(10,kernel_initializer='normal',))#was 12 nodes
    # model.add(Activation('tanh'))
    # model.add(BatchNormalization())
    # model.add(Dropout(0.15))
    # model.add(Dense(1,kernel_initializer='normal'))
    # model.add(Activation('tanh'))
    #model.summary()
    model.add(Dense(16, input_dim=7, kernel_initializer='normal'))
    model.add(Dense(10, kernel_initializer='normal'))
    model.add(Dense(1, kernel_initializer='normal'))
    tbCallBack = keras.callbacks.TensorBoard(log_dir='/tmp/keras_logs', write_graph=True)
    model.compile(loss='mean_squared_error', optimizer='Adam', metrics=['mean_squared_error'])#was rmsprop instead of Adam
    model.fit(x_train.values, y_train.values, epochs=100, batch_size=4,  verbose=1, validation_split=0.3,callbacks=[tbCallBack])
    pdn= model.predict(x_test.values)
    act = unscale(y_test,scaler)
    pdn = unscale(pdn,scaler)

    mse = mean_squared_error(act, pdn)
    mae = mean_absolute_error(act, pdn)
    print("neural network\nrmse: (%.2f) mae:(%.2f) " % (mse**0.5, mae))
    plt.scatter(act,pdn, s=1, color="blue")
    plt.title("Surge prediction results using Neural")
    plt.xlabel("y_test")
    plt.ylabel("pdn")
    plt.savefig("{}_nn_results.png".format(city,))

    dfRes = pd.DataFrame(np.column_stack([mse, mae]), columns=['MSE', 'MAE'])
    dfPdn = pd.DataFrame(np.column_stack([pdn,act]),columns=['pdn','act'])
    dfPdn.to_csv("{}_nn_pdn.csv".format(city,))

def unscale(dataset, scaler):
    zeros = np.zeros(len(dataset))
    padded = np.column_stack((zeros, zeros, zeros, zeros, zeros, zeros, zeros, dataset))
    inverted = scaler.inverse_transform(padded)
    inverted = inverted[:,7]
    return inverted

# #This first block does all of the preprocessing. Comment out if datasets have been pre-generated.
# cur=conn.cursor()
# df = get_surge_obs(surge_miles)
# df1 = get_hurdat_obs(hur_miles)
# conn.close()
# #Merge surgedat and hurdat and drop irrelevant columns
# df2 = pd.merge(df,df1,on=['storm_name','year'],how='inner')
# df2 = df2.drop(['storm_id','surge_id','date','time','waves_ft','waves_m','datum','obs_type','location', 'state','basin','conf','surge_loc','obsdate','obstime','obsnote','strength','ne34','se34','sw34','nw34','ne50','se50','sw50','nw50','ne64','se64','sw64','nw64','scode','hurdat_loc'], axis=1)
# df2 = set_target(df2)
# df2 = get_elevation(df2)

df2 = pd.read_csv('./{}_Surge_Dataset.csv'.format(city,))

#seed
seed = 7
np.random.seed(seed)

#final dataset preparation
full_df = df2[df2['pressure'] > 0]
full_df = full_df[['lat_x', 'lon_x', 'wspeed', 'pressure', 'lat_y', 'lon_y','elevation','target']]
print(full_df.head())
scaler=MinMaxScaler()
full_df = pd.DataFrame(scaler.fit_transform(full_df),columns=full_df.columns)
print(full_df.head())
df_Y = full_df[['target']]
print(df_Y.head())
df_X = full_df[['lat_x', 'lon_x', 'wspeed', 'pressure', 'elevation','lat_y', 'lon_y']]
print(df_X.head())

for i in range(0,1):
    #Split into train and test sets
    X_train, X_test, Y_train, Y_test = train_test_split(df_X, df_Y, test_size=0.33)
    print(X_train.shape, "-X_train")
    print(X_test.shape, "-X_test")
    print(Y_train.shape, "-Y_train")
    print(Y_test.shape, "-Y_test")

    #model calling
    # m_svr(X_train,X_test,Y_train,Y_test,scaler)
    nn(X_train,X_test,Y_train,Y_test,scaler)
