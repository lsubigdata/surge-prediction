%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% ICML 2013 EXAMPLE LATEX SUBMISSION FILE %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Use the following line _only_ if you're still using LaTeX 2.09.
%\documentstyle[icml2013,epsf,natbib]{article}
% If you rely on Latex2e packages, like most moden people use this:
\documentclass{article}

% For figures
\usepackage{graphicx} % more modern
%\usepackage{epsfig} % less modern
\usepackage{subfigure} 

% For citations
\usepackage{natbib}

% For algorithms
\usepackage{algorithm}
\usepackage{algorithmic}

% For symbols (added by authors)
\usepackage{gensymb}
% As of 2011, we use the hyperref package to produce hyperlinks in the resulting PDF. If this breaks your system, please commend out the following usepackage line and replace \usepackage{icml2013} with \usepackage[nohyperref]{icml2013} above.
\usepackage{hyperref}

% Packages hyperref and algorithmic misbehave sometimes. We can fix this with the following command.
\newcommand{\theHalgorithm}{\arabic{algorithm}}

% Employ the following version of the ``usepackage'' statement for submitting the draft version of the paper for review. This will set the note in the first column to ``Under review. Do not distribute.''
\usepackage[accepted]{icml2013} 
% Employ this version of the ``usepackage'' statement after the paper has been accepted, when creating the final version. This will set the note in the first column to ``Proceedings of the...'' \usepackage[accepted]{icml2013}

% The \icmltitle you define below is probably too long as a header. Therefore, a short form for the running title is supplied here:
\icmltitlerunning{Predicting Storm Surge with Machine Learning}

\begin{document} 

\twocolumn[
\icmltitle{Predicting Tropical Cyclone Storm Surge with Machine Learning}

% It is OKAY to include author information, even for blind submissions: the style file will automatically remove it for you unless you've provided the [accepted] option to the icml2013 package.
\icmlauthor{Eric Rohli}{eric@srcc.lsu.edu}
\icmladdress{NOAA Southern Regional Climate Center and College of Engineering, Louisiana State University, Baton Rouge, LA 70803 USA}
\icmlauthor{Dineep Thomas}{dthomas@srcc.lsu.edu}
\icmladdress{NOAA Southern Regional Climate Center and College of Engineering, Louisiana State University, Baton Rouge, LA 70803 USA}

% You may provide any keywords that you find helpful for describing your paper; these are used to populate the "keywords" metadata in the PDF but will not be shown in the document
\icmlkeywords{storm surge, tropical cyclone, machine learning, support vector regression, prediction}

\vskip 0.3in
]

\begin{abstract} 
Storm surge is an ocean-based threat to life and property caused by a sudden rise of seawater due to tropical cyclone landfall. Predicting storm surge is a complex problem that is inherently difficult due to varying coastal land features and hurricane characteristics such as intensity, forward speed, storm radius, and sea level pressure. This project uses machine learning techniques to analyze historical hurricane characteristics and storm surge data to predict storm surge levels along the United States coastline. Hurricane characteristics data from the National Hurricane Center's (NHC's) HURDAT2 database and storm surge data from the National Oceanic and Atmospheric Administration (NOAA) Southern Regional Climate Center's (SRCC's) SURGEDAT database are utilized in this study. The HURDAT2 dataset contains 49,691 observations from 1,830 distinct cyclones while the SURGEDAT dataset contains 7,423 storm surge observations from 260 distinct storms. These two datasets were merged during pre-processing. The resulting dataset was large, so the authors decided to focus their efforts on the New Orleans, Louisiana, USA area. Since storm surge is a continuous variable, regression-based machine learning methodologies such as support vector regression (SVR) and artificial neural networks (ANN) are needed to predict storm surge levels. The predicted surge levels are evaluated for accuracy using root mean square error and mean absolute error. SVR reduced error by 50\% when compared to ANN and was thus chosen as the preferred method of analysis for this study. The authors note that this does not imply superiority of the SVR model for all cases along the United States coastline.

\end{abstract} 
%Make sure that we mention all accuracy metrics

\section{Introduction}
Tropical cyclones are massive storms that have the potential to devastate coastal areas and towns through heavy rain, high winds, tornadoes, and flooding. Low central pressure in tropical cyclones allow the storm to change normally-stable ocean water by creating choppy seas and fuel the precipitation processes. As the storm approaches land, coastal bathymetry forces excess water near the center of the storm upward, creating large waves known as storm surge. Much of the destruction caused by tropical cyclones is directly linked to storm surge and flooding.

One of the best known examples of devastation incurred by storm surge is from 2005 Hurricane Katrina. Surge readings from Katrina were as high as 33.1 feet (10.1 meters) at an unnamed location near Waveland, Mississippi \cite{surge18}. Over 80\% of New Orleans flooded during the storm, and other evidence of high storm surge was prevalent at locations along the Gulf of Mexico coastline. Signs of damage included gutted lower floors of large buildings and refrigerators stuck in trees \cite{fritz07}. As a whole, Hurricane Katrina directly led to over 1,000 deaths and \$31 billion in property damage in Alabama, Arkansas, Florida, Louisiana, and Mississippi \cite{ncei18}.

Accurate storm surge predictions can play an important decision-support role in pre-storm proceedings. Evacuation decisions need to be made as early as possible to ease potential issues with mass exodus from threatened cities. If evacuations are ordered, residents need time to perform preventative measures such as boarding windows and laying sandbags. Mandatory evacuation notices for Hurricane Katrina were not issued until 20 hours before landfall. Heavy traffic during evacuation and lack of preparation made many gulf coast residents decide to stay at home. Furthermore, many residents who decided to leave at the last minute did not prepare their homes for the impending storm, thereby increasing the human and property losses from Katrina.

Prediction models for storm surge tend to take one of two forms. Mechanistic models use atmospheric and hydrodynamic variables to simulate the flow of water and determine maximum surge based on extremes from model runs whereas empirical models use hurricane state data and, on occasion, some atmospheric and hydrodynamic variables to predict surge heights. Due to the detail required to make the simulations, mechanistic models tend to be more accurate than empirical models at the cost of computational efficiency. Depending on the provided parameters, mechanistic models can take more than a day to produce simulated results.

Common tools used to employ mechanistic models include 3-D Finite Volume Community Ocean Model (FVCOM), unstructured grids, and Advanced Circulation (ADCIRC) models. \citet{rego10} successfully modeled storm tides along the Texas and Louisiana coastline during 2008's Hurricane Ike using FVCOM. This work was expanded by \citet{allahdadi18} to include Louisiana shelf simulations during Hurricane Katrina. \citet{shen06} attempted to recreate storm surge readings from Virginia during Hurricane Isabel (2003) and were successful except at one station where surge buildup was lagged by about two hours. \citet{deb16} applied ADCIRC and the Simulating Waves Nearshore (SWAN) wave model to two storms that affected Bangladesh and achieved respectable results for some coastal stations. \citet{dietsche07} also applied ADCIRC with finite element methods to simulate surge during Hurricane Hugo in 1989. While these five projects each provided satisfactory results, all required the use of post-storm data that would not be available during a real-time event. Furthermore, the papers that revealed computation time often reported model runs exceeding one day on powerful supercomputer systems \cite{dietsche07, deb16}. The authors believe that these two factors make mechanistic models impractical for decision-support during active storms.

Empirical models make assumptions to counteract the complexity of the mechanistic models. Many of these models rely on data mining techniques to make statistical predictions based on past storm data. The most commonly used data mining technique is the artificial neural network (ANN) which performs a weighted regression of input variables to produce an output prediction. Recent advances in computational power and memory allow for efficient nonlinear ANN training. Previous storm surge analyses using ANNs have been performed for sites in Canada \cite{yin13}, China \cite{ji11}, Gulf of Mexico \cite{mafi17}, Korea \cite{you09}, Indian Ocean \cite{chaudhuri15}, Italy \cite{bajo10, pasquali15}, Japan \cite{kim16}, Louisiana \cite{kim15}, North Sea \cite{tayel15}, Rhode Island \cite{hashemi16}, and Taiwan \cite{chen12, lee06, lee08, tsai14}.

Other data mining methods include random forests (RF) and support vector machines (SVM). These methods have returned mixed results compared to ANN. \citet{mafi17} applied ANN, RF, and SVM to a month of Gulf of Mexico tidal data from the 2008 Atlantic hurricane season and found that ANN gives slightly better results than the other two methods during short and long-term forecasts but RF gives better results in the medium-term (6-12 hours). On the other hand, \citet{rajasek08} found that SVM outperformed both ANN and a finite difference numerical model for storm surge prediction at a station in Taiwan. \citet{wei18} applied k-Nearest Neighbors, linear regression, model trees, ANN, and SVR to typhoon data from Taiwan and found that ANN and SVR give the best results at the cost of lower computational efficiency.

Based on the mixed results seen in the literature, the authors feel that it is important to investigate both SVR and ANN methodologies. 

Sections 2 and 3 of this document provide a mathematical introduction to the support vector regression and artificial neural network methodologies, respectively. Section 4 describes the dataset and preprocessing directives used during the preparation of the analysis. Section 5 offers preliminary results from SVR and ANN analyses which are further discussed in Section 6. Section 7 gives final thoughts and conclusions from the project.

\section{Artificial Neural Networks}
Artificial neural networks (ANN) are an alternative form of machine learning analysis that seek to model biological realities such as the workings of the human brain. The most basic ANN is the perceptron, which takes input data, performs a weighted sum based on either random initialization or training experience, adjusts the sum with a bias, and passes the resulting number through an activation function to receive an output result. Weights and biases in the perceptron model are often trained using an iterative process, such as backpropogation, that uses predicted-error-from-target to tune weights and biases for the next model run.

One of these iterative processes is known as adaptive moment estimation (or more simply, Adam). The basis for Adam is stochastic gradient descent (SGD), which uses point-based gradients to update and guide the function to a (usually locally) minimum error. SGD is prone to convergence issues, such as oscillation, that make the algorithm computationally expensive. To account for these convergence issues, Adam keeps an average of past and past-squared gradients to reduce the magnitude of adjustment when updating. These parameters are used as a form of momentum and help to converge the error faster.

Nonlinearity can be introduced into the neural network model by stacking perceptron units into multiple layers containing varying numbers of perceptrons. When this is achieved, the backpropogation algorithm needs to be tweaked to calculate prediction error at each node in the network. This network is known as a multilayer perceptron (MLP) network. 

As more layers are added to the MLP network, the computation time required for training and testing increases. Specialized hardware such as graphics processing units (GPUs) and servers are necessary to perform the analysis within a feasible time. Deep learning involves implementing this specialized hardware to train neural networks containing many hidden layers.

Other types of neural networks, such as recurrent neural networks and convolutional neural networks can be created by adjusting the network architecture. A recurrent neural network is formed when outputs from hidden or output layers from one model run are recycled as inputs for future runs. A convolutional neural network is created by dividing a two-dimension input, such as an image, into smaller, overlapping sub-networks. Such processes can be "flattened" into deep networks by expanding the number of hidden layers and delaying possible inputs (in the case of recurrent networks) or by subdividing task layers into local and general analysis. While such architectures can be useful for specific applications, they are computationally expensive. For this reason, the authors chose to use standard architectures for this project.

\section{Support Vector Regression} 
Support vector regression (SVR) is a type of non-parametric regression that is derived from the initial support vector machine algorithm devised by \citet{vapnik95}. The general goal of SVR is to find a solution to the regression function \(f(x) = x' \beta + b \) that minimizes the norm of the \(\beta\) terms \(J(\beta) = 0.5 \beta ' \beta \) with each residual \(|y_n - (x_n ' \beta + b)| \) less than a pre-defined \(\epsilon\). The pre-defined \(\epsilon\) gives a buffer zone that allows for norm minimization. Without the \(\epsilon\) term, SVR would reduce to least-squares.

To prevent overfitting, a term including a box constraint and slack values is introduced to the minimization function. Box constraint allows for a weighted penalty function. Slack values reduce the rigidity imposed by the \(\epsilon\) restriction by allowing some values to exceed the \(\epsilon\) limit. This is similar to the soft margin concept in support vector classification. Both the box constraint and slack values can be tuned to create better-fitting models. With this term added, the objective function to minimize is \(J(\beta) = 0.5 \beta ' \beta + C \sum_{n=1}^{N} (\zeta_n + \zeta_n^*)\) where \(C\) is the box constraint and \(\zeta_n\) and \(\zeta_n^*\) are the slack variables.

Non-linear SVR can be achieved through the implementation of various kernels. The most commonly used are the linear kernel \(G(x_j, x_k) = x_j ' x_k\), the Gaussian (also known as radial basis) kernel \(G(x_j, x_k) = exp(-||x_j - x_k||^2)\), and the polynomial kernel \(G(x_j, x_k) = (1 + x_j ' x_k)^q\). After applying a Lagrangian transformation and implementing the kernel function, the final objective function to minimize is \(L(\alpha) = 0.5 \sum_{i=1}^{N} \sum_{j=1}^{N} (\alpha_i - \alpha_i^*) (\alpha_j - \alpha_j^*) G(x_i, x_j) + \epsilon \sum_{i=1}^{N} (\alpha_i + \alpha_i^*) - \sum_{i=1}^{N} y_i (\alpha_i - \alpha_i^*) \) where \(\alpha_i, \alpha_i^*, \alpha_j,\) and \(\alpha_j^*\) are Lagrange multipliers.

\section{Dataset and Pre-processing}
Data were obtained from two sources. The National Hurricane Center publishes the HURDAT2 database which contains known "best track" hurricane data for all storms since 1851 \cite{nhc18}. Data provided by the HURDAT2 database include storm name, storm location (latitude and longitude), windspeed, central pressure, observation date and time, storm milestones (such as landfall time), and quadrant wind speed data (but only for storms since 2004). The NOAA Southern Regional Climate Center publishes a storm surge database called SURGEDAT \cite{surge18}. Data provided by the SURGEDAT database include storm year, storm name, surge location (latitude and longitude), and surge height. All data columns were normalized for this analysis to mitigate the effects of having data spanning multiple orders of magnitude.

In order to use the data, both the SURGEDAT and HURDAT2 datasets needed to be joined. Performing a full join on the datasets using storm year and name as join keys is computationally feasibile, but the resulting dataset is large and difficult to use. To limit the number of resulting matches, only surge observations within 50 miles and storm observations within 100 miles of the 30\degree N, 90\degree W point in New Orleans, Louisiana, USA, were considered. The resulting dataset contains 1,712 surge observations from 22 storms between 1901 and 2012. 

\citet{takagi16} argue for the use of a radius of maximum wind parameter when predicting storm surge. Radius of maximum wind data is not directly provided by HURDAT2, but Takagi and Wu derived an empirical relationship between the radius of maximum wind and 50 knot wind radius data. The authors decided to focus on local prediction models, so adding an additional input restriction that requires storms to have occurred since 2004 greatly reduces the amount of data on which the model can be trained and tested. While not used in this analysis, the authors recommend inclusion of wind radius for analysis of recent storm events.

\section{Results}
Artificial neural network and support vector machine models for predicting storm surge were generated and tuned. Selected error criteria were mean absolute error (MAE) and root mean squared error (RMSE), which both produce error values in the unit of the target value. Each model was run for 100 iterations with summary statistics of all model runs provided in Tables \ref{tbl1}. All model iterations were conducted with a random split where two-thirds of the dataset was used for training and one-third of the dataset was used for testing. 

\begin{table}
	\begin{center}
		\begin{tabular}{c|c|c}
			\textbf{Statistic} & 
			\textbf{ANN} & 
			\textbf{SVR} 
			\\
			\hline
			\hline
			Mean MAE (ft) & 
			0.437 &
			0.162
			\\
			\hline
			MAE Standard Deviation &
			0.103 &
			0.007 
			\\
			\hline
			\hline
			Mean RMSE (ft) &
			0.564 &
			0.237 
			\\
			\hline
			RMSE Standard Deviation &
			0.103 &
			0.012
		\end{tabular}
		\caption{Neural network and support vector model results}
		\label{tbl1}
	\end{center}
\end{table}

\subsection{Artificial Neural Network}
The artificial neural network model was built using the Keras python library. Tensorboard was also included to trace dataflow and error during training. 

The network follows the architecture in Figure \ref{fig1}. Input data is passed through two "Dense" layers (Keras-speak for a "regular" layer of neurons without tricks and gimmicks) before undergoing batch normalization. Data is then divided with some data going directly to another batch normalization module with other data experiencing "dropout" to reduce overfitting. At the second batch normalization module, the model is evaluated using validation data. At each layer, the hyperbolic tangent activation function is used to keep neuron inputs on the interval [-1, 1] instead of potentially covering multiple orders of magnitude. This process is repeated for 50 epochs with a batch size of 2.

\begin{figure}
	\includegraphics[width=\linewidth]{mlTensorboard.png}
	\caption{Tensorboard diagram showing artificial neural network architecture}
	\label{fig1}
\end{figure}

Figure \ref{fig2} plots predicted values (pdn) against actual values (y\_test) to assess goodness of model fit. For the neural network, storm surges tend to be overpredicted at lower ground truth values. Errors can be on the magnitude of 5 feet or more. As the actual value increases, the model shows signs of better fit. 

\begin{figure}
	\includegraphics[width=\linewidth]{ANNbestfit.png}
	\caption{Prediction vs. ground truth, artificial neural network model. Best fit line is given.}
	\label{fig2}
\end{figure}

As seen in Table \ref{tbl1}, the average mean absolute error for this neural model is 0.437 feet and the average root mean squared error is 0.564 ft. With an average surge observation of 8.74 feet across the entire dataset, the percent error using MAE is 5.00\% and the percent error using RMSE is 6.45\%.

\subsection{Support Vector Machines}
The support vector regression model was built using the scikit-learn python library. Within scikit-learn, the svm module allows you to run SVR. Data from the initial catalog is randomly split into train and test sets and passed into an SVR training model that uses the radial basis function. Scikit-learn fits the model, makes predictions, and calculates error. Errors from 100 random initializations are averaged to produce the overall SVR error. 

Figure \ref{fig3} shows a goodness of fit plot for one iteration of SVR. This plot shows a strong linear fit between the predicted model and the ground truth. However, Table \ref{tbl1} suggests that the SVR model is not significantly different than the ANN model. This may due to occasional extreme errors. For instance, one point in the given SVR iteration is predicted at approximately 15 feet whereas the ground truth value for that point is approximately 7 feet.

\begin{figure}
	\includegraphics[width=\linewidth]{SVRbestfit.png}
	\caption{Prediction vs. ground truth, support vector regression model. Best fit line is given.}
	\label{fig3}
\end{figure}

Table \ref{tbl1} lists an average MAE of 0.162 ft and an RMSE of 0.237 ft for the SVR model. Considering the average surge observation of 8.74 ft, the percent error of the SVR model when considering MAE is 1.85\% and when considering RMSE, the percent error is 2.71\%. 

\section{Discussion}
A Student's t test was performed to compare the results of the support vector regression model and the artificial neural network model. The MAE and RMSE reductions incurred by using the SVR model are both statistically significant (p $<$ 0.0001). The literature review in the Introduction to this study references an abundance of storm surge models that employ artificial neural networks but few studies that employ support vector machines. The results of this paper suggest that the SVR methodology is certainly competitive to, and in some cases better than, the ANN methodology when predicting storm surge. The results do not necessarily imply that SVR will always produce lower error than ANN for predicting storm surge.

The results shown suggest that the SVR model can be implemented in an early warning system for emergency preparedness purposes. With average surge prediction errors less than 0.25 feet, civic leaders will have a mostly-accurate baseline for the damage potential due to storm surge. This allows decisions to be made sooner than when using mechanistic models that require hours of runtime. Furthermore, the level of accuracy of projections allows civic leaders to compare storm surge with recent storms and better communicate the threat of storm surge to the public. 

It should be noted that while the average SVR error is less than 0.25 feet, there are cases where predictive error exceeds 8 feet or more. All model results should be evaluated for context. For example, one prediction in Figure \ref{fig2} gave a storm surge prediction close to -5 feet while the ground truth value was actually around 6 feet. While it is possible for a storm to have draining effects, these events are not common in coastal Louisiana and the negative predictive value should immediately suggest unreliability.

\section{Conclusion}
This paper describes the creation of an artificial neural network and a support vector regression model for predicting storm surge around the New Orleans, Louisiana, USA area. All data used in this project were obtained from openly available sources, including the HURDAT2 database issued by the National Weather Service and the SURGEDAT database issued by the Southern Regional Climate Center. The SVR model provided a statistically significant error reduction over the ANN model. Further analysis of ANN and SVR models at other points along United States coastlines along the Gulf of Mexico and Atlantic Ocean can provide validation for the results of this study.

\bibliography{example_paper}
\bibliographystyle{icml2013}

\end{document} 

% This document was modified from the file originally made available by Pat Langley and Andrea Danyluk for ICML-2K. This version was created by Lise Getoor and Tobias Scheffer, it was slightly modified from the 2010 version by Thorsten Joachims & Johannes Fuernkranz, slightly modified from the 2009 version by Kiri Wagstaff and Sam Roweis's 2008 version, which is slightly modified from Prasad Tadepalli's 2007 version which is a lightly changed version of the previous year's version by Andrew Moore, which was in turn edited from those of Kristian Kersting and Codrina Lauth. Alex Smola contributed to the algorithmic style files. 
