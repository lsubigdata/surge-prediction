import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time

from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.linear_model.stochastic_gradient import SGDRegressor
from sklearn import linear_model
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor

def main(parsed_args):
    df = pd.read_csv('./TRIMMEDspatialJoin50miSurge100miHurFromLat30.0Lon-90.0.csv')
    df = df[['slat', 'slon', 'hwspeed', 'hpressure', 'hlat', 'hlon', 'target']]
    print(len(df))
    df = df[df['hpressure'] > 0]
    print(len(df))
    df.to_csv("beforenormalized.csv",header=True,sep=',')
    scaler = MinMaxScaler()

    df['logtarget'] = np.log(df['target'])
    df2 = pd.DataFrame(scaler.fit_transform(df), columns=df.columns)
    dfX = df2[['slat', 'slon', 'hwspeed', 'hpressure', 'hlat', 'hlon']]
    print(df2.head())
    #We should probably create a new column for "time before landfall"
    dfY = df2['logtarget']
    #dfY = df2['target']
    df2.to_csv("normalized.csv",header=True,sep=',')

    #sgd(dfX, dfY, parsed_args)
    #lasso(dfX, dfY, parsed_args)
    #ridge(dfX, dfY, parsed_args)
    svr(dfX, dfY, parsed_args)
    #rf(dfX, dfY, parsed_args)


def sgd(dfx, dfy, parsed_args):
    lossFn = parsed_args.l
    penFn = parsed_args.p

    rs = []
    cvr2 = []
    cvmse = []
    cvmae = []

    for i in range(0, parsed_args.n+1):
        print("SGD with Loss Function " + lossFn + ", Penalty Function: " + penFn + " Iteration: " + str(i) + " started.")
        X_train, X_test, y_train, y_test = train_test_split(dfx, dfy, test_size=1.0/3.0, random_state=i)

        clf = SGDRegressor(loss=lossFn, penalty=penFn)
        clf.fit(X_train, y_train)
        pdn = clf.predict(X_test)

        rs.append(i)
        cvr2.append(r2_score(y_test, pdn))
        cvmse.append(mean_squared_error(y_test, pdn))
        cvmae.append(mean_absolute_error(y_test, pdn))

    plt.scatter(y_test,pdn, s=1, color="blue")
    plt.title("Surge prediction results using SGD")
    plt.xlabel("y_test")
    plt.ylabel("pdn")
    plt.savefig("./images/SGD_results.png")
    plt.close()
    opath = './results/SGD_' + parsed_args.l + '_' + parsed_args.p + '_' + str(parsed_args.n) + 'Iterations.csv'
    dfRes = pd.DataFrame(np.column_stack([rs, cvr2, cvmse, cvmae]), columns=['Iter', 'R2', 'MSE', 'MAE'])
    dfRes.to_csv(opath)

def lasso(dfx, dfy, parsed_args):
    rs = []
    cvr2 = []
    cvmse = []
    cvmae = []

    if parsed_args.a >=0.0 and parsed_args.a <=1.0:
        for i in range(0, parsed_args.n+1):
            print("Lasso with Alpha " + str(parsed_args.a) + " Iteration: " + str(i) + " started.")
            X_train, X_test, y_train, y_test = train_test_split(dfx, dfy, test_size=1.0/3.0, random_state=i)

            clf = linear_model.Lasso(alpha=parsed_args.a)
            clf.fit(X_train, y_train)
            pdn = clf.predict(X_test)

            rs.append(i)
            cvr2.append(r2_score(y_test, pdn))
            cvmse.append(mean_squared_error(y_test, pdn))
            cvmae.append(mean_absolute_error(y_test, pdn))

        plt.scatter(y_test,pdn, s=1, color="blue")
        plt.title("Surge prediction results using lasso")
        plt.xlabel("y_test")
        plt.ylabel("pdn")
        plt.savefig("./images/lasso_results.png")
        plt.close()
        opath = './results/Lasso_' + str(parsed_args.a) + '_' + str(parsed_args.n) + 'Iterations.csv'
        dfRes = pd.DataFrame(np.column_stack([rs, cvr2, cvmse, cvmae]), columns=['Iter', 'R2', 'MSE', 'MAE'])
        dfRes.to_csv(opath)
    else:
        print("Out of range error. Alpha must be between 0 and 1, inclusive.")

def ridge(dfx, dfy, parsed_args):
    rs = []
    cvr2 = []
    cvmse = []
    cvmae = []

    if parsed_args.a >=0.0 and parsed_args.a <=1.0:
        for i in range(0, parsed_args.n+1):
            print("Ridge with Alpha " + str(parsed_args.a) + " Iteration: " + str(i) + " started.")
            X_train, X_test, y_train, y_test = train_test_split(dfx, dfy, test_size=1.0/3.0, random_state=i)

            clf = linear_model.Ridge(alpha=parsed_args.a)
            clf.fit(X_train, y_train)
            pdn = clf.predict(X_test)

            rs.append(i)
            cvr2.append(r2_score(y_test, pdn))
            cvmse.append(mean_squared_error(y_test, pdn))
            cvmae.append(mean_absolute_error(y_test, pdn))

        #plt.scatter(X_train,y_train,color="red")
        plt.scatter(y_test,pdn, s=1, color="blue")
        plt.title("Surge prediction results using ridge regression")
        plt.xlabel("y_test")
        plt.ylabel("pdn")
        plt.savefig("./images/Ridge_Results.png")
        plt.close()
        opath = './results/Ridge_' + str(parsed_args.a) + '_' + str(parsed_args.n) + 'Iterations.csv'
        dfRes = pd.DataFrame(np.column_stack([rs, cvr2, cvmse, cvmae]), columns=['Iter', 'R2', 'MSE', 'MAE'])
        dfRes.to_csv(opath)
    else:
        print("Out of range error. Alpha must be between 0 and 1, inclusive.")

def svr(dfx, dfy, parsed_args):
    rs = []
    cvr2 = []
    cvmse = []
    cvmae = []

    for i in range(0, parsed_args.n+1):
        print("SVR with " + parsed_args.k + " Kernel Iteration: " + str(i) + " started.")
        X_train, X_test, y_train, y_test = train_test_split(dfx, dfy, test_size=1.0/3.0, random_state=i)

        #Cs = [10]
        #gammas = [0.96]
        #param_grid={'C':Cs, 'gamma':gammas}
        #grid_search = GridSearchCV(SVR(kernel=parsed_args.k),param_grid,cv=10)
        #grid_search.fit(X_train, y_train)
        #grid_search.best_params_
        clf = SVR(kernel='rbf', C=10, gamma=0.96,shrinking=True,epsilon=0.0001)
        clf.fit(X_train, y_train)
        pdn = clf.predict(X_test)
        #pdn = grid_search.predict(X_test)
        #print("estimator:: ",grid_search.best_estimator_)
        #print("Score: " + str(grid_search.best_estimator_.score(X_test,y_test)))
        rs.append(i)
        cvr2.append(r2_score(y_test, pdn))
        cvmse.append(mean_squared_error(y_test, pdn))
        cvmae.append(mean_absolute_error(y_test, pdn))

    #plt.scatter(X_train,y_train,color="red")
    plt.scatter(y_test,pdn, s=1, color="blue")
    plt.title("Surge prediction results using SVR")
    plt.xlabel("y_test")
    plt.ylabel("pdn")
    plt.savefig("./images/SVR_results.png")
    plt.close()

    opath = './results/0.96LogSVR_' + parsed_args.k + '_' + str(parsed_args.n) + 'Iterations.csv'
    dfRes = pd.DataFrame(np.column_stack([rs, cvr2, cvmse, cvmae]), columns=['Iter', 'R2', 'MSE', 'MAE'])
    dfRes.to_csv(opath)

def rf(dfx, dfy, parsed_args):
    rs = []
    cvr2 = []
    cvmse = []
    cvmae = []

    for i in range(0, parsed_args.n+1):
        print("Random Forest Iteration: " + str(i) + " started.")
        X_train, X_test, y_train, y_test = train_test_split(dfx, dfy, test_size=1.0/3.0, random_state=i)

        clf = RandomForestRegressor(n_estimators=10, criterion='mse')
        clf.fit(X_train, y_train)
        pdn = clf.predict(X_test)

        rs.append(i)
        cvr2.append(r2_score(y_test, pdn))
        cvmse.append(mean_squared_error(y_test, pdn))
        cvmae.append(mean_absolute_error(y_test, pdn))

    plt.scatter(y_test,pdn, s=1, color="blue")
    plt.title("Surge prediction results using RF")
    plt.xlabel("y_test")
    plt.ylabel("pdn")
    plt.savefig("./images/RF_results.png")
    plt.close()
    opath = './results/RF_' + str(parsed_args.n) + 'Iterations.csv'
    dfRes = pd.DataFrame(np.column_stack([rs, cvr2, cvmse, cvmae]), columns=['Iter', 'R2', 'MSE', 'MAE'])
    dfRes.to_csv(opath)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run classification for surgedb.')
    parser.add_argument('-a', type=float, default=0.05, help="Alpha for Lasso and Ridge. Must be a number on the interval [0,1]. Default is 0.05")
    parser.add_argument('-c', type=str, default='SVR', help="Classifier. Use scikit-learn notation. Default is SVR.")
    parser.add_argument('-k', type=str, default='rbf', help='Kernel function for SVR. Use scikit-learn notation. Default is rbf.')
    parser.add_argument('-l', type=str, default='squared_loss', help="Loss function for SGDRegressor. Use scikit-learn notation. Default is squared_loss.")

    parser.add_argument('-n', type=int, default=100, help='Number of iterations to run. Must be an integer. Default is 100.')
    parser.add_argument('-p', type=str, default='l2', help="Penalty function for SGDRegressor. Use scikit-learn notation. Default is l2")
    parsed_args = parser.parse_args()
    main(parsed_args)
